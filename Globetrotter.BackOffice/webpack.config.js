﻿const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HardSourceWebpackPlugin = require("hard-source-webpack-plugin");

module.exports = (env = {}, argv = {}) => {
    const isDevelopment = process.env.NODE_ENV !== "production";
    console.log(process.env.NODE_ENV);
    return {
		mode: isDevelopment ? "development" : "production",
		entry: { "main": path.join(__dirname, "content/src/js/site.js") },
		output: {
			path: path.join(__dirname, "wwwroot/dist"),
			filename: isDevelopment ? "[name].js" : "[name].[contenthash].js",
			publicPath: "/dist/"
        },
		module: {
			rules: [
				{
					test: /\.(woff|woff2|eot|ttf)$/,
					use: [
						{
							loader: "file-loader",
							options: {
								name: "[name].[ext]",
								outputPath: "../font/"
							}
						}
					]
				},
				{
					test: /\.js$/,
					use: "babel-loader",
					exclude: [/node_modules/]
				},
				{
					test: /\.(sass|scss)$/,
					use: [
						{ loader: MiniCssExtractPlugin.loader },
						{ loader: "css-loader" },
						{ loader: "sass-loader", options: { includePaths: ["node_modules"] } }
					]
				}
			]
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: path.resolve(__dirname, "views/shared/_Layout_template.cshtml"),
                filename: path.resolve(__dirname, "views/shared/_Layout.cshtml")
			}),
			new MiniCssExtractPlugin({
				filename: isDevelopment ? "[name].css" : "[name].[contenthash].css"
			}),
            new CleanWebpackPlugin(),
            new HardSourceWebpackPlugin()
		],
		optimization: {
			runtimeChunk: "single",
				splitChunks: {
					cacheGroups: {
						vendor: {
							test: /[\\/]node_modules[\\/]/,
							name: "vendors",
							chunks: "all"
						}
					}
				},
		minimizer: [
			new UglifyJsPlugin({
				cache: true,
				parallel: true,
				sourceMap: true,
                uglifyOptions: {
                    output: {
                        comments: false
                    }
                }
            }),
			new OptimizeCssAssetsPlugin({})
			]
		},
        resolve: {
            extensions: [".js", ".scss"],
            alias: {
                styles: path.resolve(__dirname, "content/src/sass/site"),
                scripts: path.resolve(__dirname, "content/src/js")
            }
        }
	};
};