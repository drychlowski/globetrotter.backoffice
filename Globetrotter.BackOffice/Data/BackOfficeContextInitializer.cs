﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Globetrotter.BackOffice.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace Globetrotter.BackOffice.Data
{
    public class BackOfficeContextInitializer
    {
        private readonly BackOfficeContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public BackOfficeContextInitializer(BackOfficeContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task Initialize()
        {
            await AddAdmin();
            await AddContinents();
        }

        private async Task AddAdmin()
        {
            var isAdminExist = _userManager.Users.Any(u => u.UserName.Equals("admin@globetrotter.com"));

            if (!isAdminExist)
            {
                var adminIdentity = new ApplicationUser {UserName = "admin@globetrotter.com", Email = "admin@globetrotter.com", ApplicationId = Application.BackOffice.Id};
                await _userManager.CreateAsync(adminIdentity, "Admin@123");
                await _context.AddAsync(new BackOfficeUser {ApplicationUser = adminIdentity});
                await _context.SaveChangesAsync();
            }
        }

        private async Task AddContinents()
        {
            var continentsToAdd = new List<Continent>
            {
                new Continent {Name = "Europa"},
                new Continent {Name = "Azja"},
                new Continent {Name = "Afryka"},
                new Continent {Name = "Ameryka południowa"}
            };

            var continentsNotInDatabase = continentsToAdd.Where(c => !_context.Continents.Select(x => x.Name).Contains(c.Name)).ToList();
            
            await _context.AddRangeAsync(continentsNotInDatabase);
            await _context.SaveChangesAsync();
        }
    }
}