﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Reservation : Entity
    {
        public decimal TotalCost { get; set;}
        public Contact Contact { get; set; }
        public int ReservationStatusId { get; set; }
        public virtual ReservationStatus Status { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public int TripId { get; set; }
        public virtual Trip Trip { get; set; }
        public int? ApplicationUserId { get; set; }
        public virtual FrontOfficeUser FrontOfficeUser { get; set; }
    }
}