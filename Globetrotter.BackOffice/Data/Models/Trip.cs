﻿using System;
using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Trip : Entity
    {
        public Price Price { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public int EmptySlots { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }
    }
}