﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Price : ValueObject<Price>
    {
        public decimal BasePrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public PriceType Type { get; set; }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return BasePrice;
            yield return CurrentPrice;
            yield return Type;
        }
    }
}