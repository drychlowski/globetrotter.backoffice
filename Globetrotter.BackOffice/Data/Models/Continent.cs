﻿using System;
using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Continent : Entity
    {
        public string Name { get; set; }
        public ICollection<Country> Countries { get; set; }

        public Continent()
        {
        }

        public Continent(int id, DateTime creationDate) : base(id, creationDate)
        {
        }
    }
}