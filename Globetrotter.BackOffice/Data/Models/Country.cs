﻿using System;
using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Country : Entity
    {
        public string Name { get; set; }
        public int ContinentId { get; set; }
        public virtual Continent Continent { get; set; }
        public virtual ICollection<OfferCountry> Offers { get; set; }

        public Country()
        {
        }

        public Country(int id, DateTime creationDate) : base(id, creationDate)
        {
        }
    }
}