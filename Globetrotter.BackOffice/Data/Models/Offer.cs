﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Offer : Entity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string TravelPlan { get; set; }
        public string Accommodation { get; set; }
        public int Nights { get; set; }
        public virtual ICollection<OfferCountry> Countries { get; set; }
        public virtual ICollection<OfferImage> OfferImages { get; set; }
        public virtual ICollection<Trip> Trips { get; set; }
    }
}