﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Application : Enumeration<Application>
    {
        public static Application FrontOffice = new Application(1, nameof(FrontOffice));
        public static Application BackOffice = new Application(2, nameof(BackOffice));

        public Application(int id, string name) : base(id, name)
        {
        }

        public static IEnumerable<Application> GetAll()
        {
            yield return FrontOffice;
            yield return BackOffice;
        }
    }
}