﻿using System;
using System.Collections.Generic;

namespace Globetrotter.BackOffice.Data.Models.Core
{
    public abstract class Enumeration<T> : ValueObject<T>
    {
        public int Id { get; }
        public string Name { get; }

        protected Enumeration(int id, string name)
        {
            Id = id;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        protected sealed override IEnumerable<object> GetEqualityComponents()
        {
            yield return Id;
            yield return Name;
        }
    }
}