﻿using Globetrotter.BackOffice.Models;

namespace Globetrotter.BackOffice.Data.Models.Core
{
    public abstract class ValueObject<T> : Equatable<ValueObject<T>>
    {
    }
}