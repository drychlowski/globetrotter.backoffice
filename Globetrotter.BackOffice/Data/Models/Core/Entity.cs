﻿using System;

namespace Globetrotter.BackOffice.Data.Models.Core
{
    public abstract class Entity
    {
        public int Id { get; private set; }
        public DateTime CreationDate { get; private set; }

        protected Entity()
        {
            CreationDate = DateTime.Now;
        }

        protected Entity(int id, DateTime creationDate)
        {
            Id = id;
            CreationDate = creationDate;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Entity other))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != other.GetType())
                return false;

            if (Id == 0 || other.Id == 0)
                return false;

            return Id == other.Id;
        }

        public static bool operator ==(Entity a, Entity b)
        {
            if (a is null && b is null)
                return true;

            if (a is null || b is null)
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Entity a, Entity b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return (GetType().ToString() + Id).GetHashCode();
        }
    }
}