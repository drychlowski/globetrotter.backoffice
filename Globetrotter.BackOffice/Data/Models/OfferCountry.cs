﻿namespace Globetrotter.BackOffice.Data.Models
{
    public class OfferCountry
    {
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}