﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class PriceType : Enumeration<PriceType>
    {
        public static PriceType Standard = new PriceType(1, nameof(Standard));
        public static PriceType Promotion = new PriceType(2, nameof(Promotion));
        public static PriceType FirstMinute = new PriceType(3, nameof(FirstMinute));
        public static PriceType LastMinute = new PriceType(4, nameof(LastMinute));

        public PriceType(int id, string name) : base(id, name)
        {
        }
        public static IEnumerable<PriceType> GetAll()
        {
            yield return Standard;
            yield return Promotion;
            yield return FirstMinute;
            yield return LastMinute;
        }
    }
}