﻿using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Customer : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public string PassportNumber { get; set; }
        public int ReservationId { get; set; }
        public virtual Reservation Reservation { get; set; }
    }
}