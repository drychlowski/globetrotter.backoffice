﻿using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class OfferImage : Entity
    {
        public int ImageId { get; set; }
        public virtual Image Image { get; set; }
        public int OfferImageTypeId { get; set; }
        public virtual OfferImageType OfferImageType { get; set; }
    }
}