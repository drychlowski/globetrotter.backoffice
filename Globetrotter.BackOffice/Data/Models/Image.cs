﻿using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class Image : Entity
    {
        public string Path { get; set; }
        public string Name { get; set; }
    }
}