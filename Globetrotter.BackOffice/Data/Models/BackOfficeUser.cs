﻿using Globetrotter.BackOffice.Data.Models.Core;
using Microsoft.AspNetCore.Identity;

namespace Globetrotter.BackOffice.Data.Models
{
    public class BackOfficeUser : Entity
    {
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}