﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class PaymentType : Enumeration<PaymentType>
    {
        public static PaymentType TraditionalTransfer = new PaymentType(1, nameof(TraditionalTransfer));

        public PaymentType(int id, string name) : base(id, name)
        {
        }

        public static IEnumerable<PaymentType> GetAll()
        {
            yield return TraditionalTransfer;
        }
    }
}