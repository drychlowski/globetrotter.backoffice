﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class OfferImageType : Enumeration<OfferImageType>
    {
        public static OfferImageType Main = new OfferImageType(1, nameof(Main));
        public static OfferImageType Standard = new OfferImageType(2, nameof(Standard));

        public OfferImageType(int id, string name) : base(id, name)
        {
        }

        public static IEnumerable<Enumeration<OfferImageType>> GetAll()
        {
            yield return Main;
            yield return Standard;
        }
    }
}