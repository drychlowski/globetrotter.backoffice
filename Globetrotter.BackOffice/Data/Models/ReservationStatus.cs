﻿using System.Collections.Generic;
using Globetrotter.BackOffice.Data.Models.Core;

namespace Globetrotter.BackOffice.Data.Models
{
    public class ReservationStatus : Enumeration<ReservationStatus>
    {
        public static ReservationStatus Unpaid = new ReservationStatus(1, nameof(Unpaid));
        public static ReservationStatus Paid = new ReservationStatus(2, nameof(Paid));
        public static ReservationStatus Canceled = new ReservationStatus(3, nameof(Canceled));

        public ReservationStatus(int id, string name) : base(id, name)
        {
        }

        public static IEnumerable<ReservationStatus> GetAll()
        {
            yield return Unpaid;
            yield return Paid;
            yield return Canceled;
        }
    }
}