﻿using Microsoft.AspNetCore.Identity;

namespace Globetrotter.BackOffice.Data.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int ApplicationId { get; set; }
        public virtual Application Application { get; set; }
    }
}