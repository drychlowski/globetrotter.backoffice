﻿using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class OfferCountryConfiguration : IEntityTypeConfiguration<OfferCountry>
    {
        public void Configure(EntityTypeBuilder<OfferCountry> builder)
        {
            builder.HasKey(offerCountry => new {offerCountry.OfferId, offerCountry.CountryId});

            builder.HasOne(offerCountry => offerCountry.Offer)
                .WithMany(offer => offer.Countries)
                .HasForeignKey(offerCountry => offerCountry.OfferId);

            builder.HasOne(offerCountry => offerCountry.Country)
                .WithMany(country => country.Offers)
                .HasForeignKey(offerCountry => offerCountry.CountryId);
        }
    }
}