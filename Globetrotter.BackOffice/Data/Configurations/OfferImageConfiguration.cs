﻿using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class OfferImageConfiguration : IEntityTypeConfiguration<OfferImage>
    {
        public void Configure(EntityTypeBuilder<OfferImage> builder)
        {
        }
    }
}