﻿using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class FrontOfficeUserConfiguration : IEntityTypeConfiguration<FrontOfficeUser>
    {
        public void Configure(EntityTypeBuilder<FrontOfficeUser> builder)
        {
            builder.OwnsOne(user => user.Contact);
        }
    }
}