﻿using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class ReservationConfiguration : IEntityTypeConfiguration<Reservation>
    {
        public void Configure(EntityTypeBuilder<Reservation> builder)
        {
            builder.OwnsOne(reservation => reservation.Contact);

            builder.Property(p => p.TotalCost)
                .HasColumnType("decimal(18,2)");
        }
    }
}