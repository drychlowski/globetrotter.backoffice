﻿using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class ApplicationConfiguration : IEntityTypeConfiguration<Application>
    {
        public void Configure(EntityTypeBuilder<Application> builder)
        {
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Name);

            builder.HasData(Application.GetAll());
        }
    }
}