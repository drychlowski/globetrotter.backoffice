﻿using System;
using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class CountryConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            //builder.HasData(new[]
            //{
            //   new Country(1, DateTime.Now) {ContinentId = 1, Name = "Francja"}, 
            //   new Country(2, DateTime.Now) {ContinentId = 1, Name = "Niemcy"}, 
            //   new Country(3, DateTime.Now) {ContinentId = 1, Name = "Hiszpania"}, 
            //   new Country(4, DateTime.Now) {ContinentId = 1, Name = "Włochy"}, 
            //   new Country(5, DateTime.Now) {ContinentId = 1, Name = "Grecja"}, 

            //   new Country(6, DateTime.Now) {ContinentId = 2, Name = "Tajlandia"}, 
            //   new Country(7, DateTime.Now) {ContinentId = 2, Name = "Wietnam"}, 
            //   new Country(8, DateTime.Now) {ContinentId = 2, Name = "Kambodża"}, 
            //   new Country(9, DateTime.Now) {ContinentId = 2, Name = "Japonia"},
            //   new Country(10, DateTime.Now) {ContinentId = 2, Name = "Iran"},

            //   new Country(11, DateTime.Now) {ContinentId = 3, Name = "Egipt"},
            //   new Country(12, DateTime.Now) {ContinentId = 3, Name = "Tunezja"},
            //   new Country(13, DateTime.Now) {ContinentId = 3, Name = "Maroko"},
            //   new Country(14, DateTime.Now) {ContinentId = 3, Name = "Etiopia"},
            //   new Country(15, DateTime.Now) {ContinentId = 3, Name = "RPA"},

            //   new Country(16, DateTime.Now) {ContinentId = 4, Name = "Brazylia"},
            //   new Country(17, DateTime.Now) {ContinentId = 4, Name = "Chile"},
            //   new Country(18, DateTime.Now) {ContinentId = 4, Name = "Argentyna"},
            //   new Country(19, DateTime.Now) {ContinentId = 4, Name = "Kolumbia"},
            //   new Country(20, DateTime.Now) {ContinentId = 4, Name = "Peru"},
            //});
        }
    }
}