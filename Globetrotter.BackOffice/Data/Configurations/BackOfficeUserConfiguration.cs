﻿using Globetrotter.BackOffice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Globetrotter.BackOffice.Data.Configurations
{
    public class BackOfficeUserConfiguration : IEntityTypeConfiguration<BackOfficeUser>
    {
        public void Configure(EntityTypeBuilder<BackOfficeUser> builder)
        {
        }
    }
}