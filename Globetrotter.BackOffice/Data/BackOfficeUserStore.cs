﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Globetrotter.BackOffice.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Globetrotter.BackOffice.Data
{
    public class BackOfficeUserStore : UserStore<ApplicationUser>
    {
        public BackOfficeUserStore(BackOfficeContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        public override Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            return Users.Where(user => user.Application == Application.BackOffice).FirstOrDefaultAsync(u => u.NormalizedUserName == normalizedUserName, cancellationToken);
        }
    }
}