﻿import "styles";
import $ from "jquery";
import "jquery-validation";
import "jquery-validation-unobtrusive";
import "bootstrap";

import "@fortawesome/fontawesome-free/webfonts/fa-brands-400.woff";
import "@fortawesome/fontawesome-free/webfonts/fa-regular-400.woff";
import "@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff";

window.jQuery = $;
window.$ = $;
