﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Globetrotter.BackOffice.Models
{
    public abstract class Equatable<T> : IEquatable<Equatable<T>>
    {
        protected abstract IEnumerable<object> GetEqualityComponents();

        public bool Equals(Equatable<T> other)
        {
            if (other == null) return false;

            return GetType() == other.GetType() && GetEqualityComponents().SequenceEqual(other.GetEqualityComponents());
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Equatable<T>)obj);
        }

        public override int GetHashCode()
        {
            return GetEqualityComponents()
                .Aggregate(1, (current, obj) =>
                {
                    unchecked
                    {
                        return current * 23 + (obj?.GetHashCode() ?? 0);
                    }
                });
        }

        public static bool operator ==(Equatable<T> a, Equatable<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Equatable<T> a, Equatable<T> b)
        {
            return !(a == b);
        }
    }
}